var express = require('express');
var router = express.Router();
var request = require('request');
var base = require('./base');

require('dotenv').config();
var server = process.env.SERVER;
var urlBase = process.env.URLBASE;
base.urlBase = urlBase;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { urlBase: urlBase });
});

/* Evitando o redirecionamento antigo */
router.get('/cep/', function(req, res, next) {
  res.redirect(301,'/');
});

/* Lista as Cidades */
router.get('/cep/:estado', function(req, res, next) {
  console.log("Acessando o Estado: " + req.params.estado);
  var estado = req.params.estado;
  estado = estado.toLowerCase();

  //Efetua a pesquisa.
  var options = {uri: server+'cep/' + estado, method: 'GET',json: {}};
  request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log(body);
      var dadosBase = JSON.stringify(body);
      var dados = JSON.parse(dadosBase);
      base.title = dados.nome ;
      base.description = 'Clique aqui para entrar no site';
      url = req.url;
      url = url.substring(1);
      base.urlAtual = base.urlBase + url;
      res.render('estados', {base: base, body: dados});  
    }else{
      console.log(error);
      res.status(404).render('naoEncontrado'); 
    }
  });
});

/* Lista os Bairros por Estado/Cidade */
router.get('/cep/:estado/:cidade', function(req, res, next) {
  console.log("Acessando o Estado: " + req.params.estado + " e a Cidade: " + req.params.cidade);
  var estado = req.params.estado;
  estado = estado.toLowerCase();
  var cidade = req.params.cidade;
  cidade = cidade.toLowerCase();

  //Efetua a pesquisa.
  var options = {uri: server + 'cep/' + estado + "/" + cidade, method: 'GET', json: {}};
  request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log(body);
      var dadosBase = JSON.stringify(body);
      var dados = JSON.parse(dadosBase);
      base.title = "Ceps de " + dados.nome + "/" + dados.estado + ' - Muno';
      base.description = 'Informações e Ceps de ' + dados.nome + '/' + dados.estado + '. Muno - Busca Ceps e Endereços do Brasil';
      url = req.url;
      url = url.substring(1);
      base.urlAtual = base.urlBase + url;
      res.render('cidades', {base: base, body: dados});  
    }else{
      console.log(error);
      res.status(404).render('naoEncontrado'); 
    }
  });
});

/* Lista as Ruas por Estado/Cidade/Bairro */
router.get('/cep/:estado/:cidade/:bairro', function(req, res, next) {
  console.log("Estado: " + req.params.estado + ", Cidade: " + req.params.cidade + ", Bairro: " + req.params.bairro);
  var estado = req.params.estado;
  var cidade = req.params.cidade;
  var bairro = req.params.bairro;
  cidade = cidade.toLowerCase();
  estado = estado.toLowerCase();
  bairro = bairro.toLowerCase();

  //Efetua a pesquisa.
  var options =
  {
    uri: server + 'cep/' + estado + "/" + cidade + "/" + bairro,
      method: 'GET',
      json: {}
  };
  request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log(body);
      var dadosBase = JSON.stringify(body);
      var dados = JSON.parse(dadosBase);
      if(dados.cepMudou == true) {
        res.redirect(301,'/' + dados.cidadeUrl);
      }
      base.title = dados.nome + " - " + dados.cidade + "/" + dados.estado;
      base.description = 'Clique aqui para entrar no site';
      url = req.url;
      url = url.substring(1);
      base.urlAtual = base.urlBase + url;
      res.render('bairros', {base: base, body: dados});  
    }else{
      console.log(error);
      res.status(404).render('naoEncontrado'); 
    }
  });
});

/* Acessa os Dados do CEP */
router.get('/cep/:estado/:cidade/:bairro/:logradouro/:cep', function(req, res, next) {
  //console.log("Estado: " + req.params.estado + ", Cidade: " + req.params.cidade + ", Bairro: " + req.params.bairro);
  var estado = req.params.estado;
  var cidade = req.params.cidade;
  var bairro = req.params.bairro;
  var logradouro = req.params.logradouro;
  var cep = req.params.cep;
  cidade = cidade.toLowerCase();
  estado = estado.toLowerCase();
  bairro = bairro.toLowerCase();
  logradouro = logradouro.toLowerCase();

  //Efetua a pesquisa.
  var options =
  {
    uri: server + 'cep/' + estado + "/" + cidade + "/" + bairro + "/" + logradouro + "/" + cep,
      method: 'GET',
      json: {}
  };
  request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log(body);
      var dadosBase = JSON.stringify(body);
      var dados = JSON.parse(dadosBase);
      if(dados.cepMudou == true) {
        res.redirect(301,'/' + dados.cidadeUrl);
      }
      base.title = "Cep de " + dados.nome + " - " + dados.bairro + ' - ' + dados.cidade + '/' + dados.estado + ' - CEP: ' + dados.cep + ' - Muno';
      base.description = 'Informações e Cep do Endereço ' + dados.nome + ' - ' + dados.bairro + ' - ' + dados.cidade + '/' + dados.estado + ' - CEP: ' + dados.cep + '. Muno - Busca Ceps e Endereços do Brasil';
      url = req.url;
      url = url.substring(1);
      base.urlAtual = base.urlBase + url;

      res.render('ceps', {base: base, body: dados});  
    }else{
      console.log(error);
      res.status(404).render('naoEncontrado');
    }
  });
});

/* pesquisar */
router.post('/pesquisar', function(req, res, next) {
  console.log(req.body);
  var endereco = req.body.endereco;
  if(endereco.length < 5) {
    res.render('index', {erroTamanho: endereco});
  }
  //Efetua a pesquisa.
  console.log(server+"pesquisar");
  var options =
  {
    uri: server+'pesquisar/',
      method: 'POST',
      json: {
        "endereco": endereco
      }
  };
  request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log(body);
      var dadosBase = JSON.stringify(body);
      var dados = JSON.parse(dadosBase);

      /* Se o CEP estiver com somente um resultado */
      if(dados.totalCeps === 1) {
        res.redirect(301,'/' + dados.ceps[0].url);
        console.log("Vamos direcionar para o CEP " + dados.ceps[0].url);
      }

      if(dados.totalCeps === 0 && dados.totalBairros === 0 && dados.totalCidades === 0 && dados.totalEnderecos === 0) {
        res.render('index', {erroEndereco: endereco});
      }else{
        base.title = "Muno - Resultado da Pesquisa";
        base.description = "Clique aqui para entrar no site";
        url = req.url;
        url = url.substring(1);
        base.urlAtual = base.urlBase + url;

        res.render('resultadoPesquisa', {base: base, body: dados, urlBase: urlBase});  
      }

    }else{
      console.log(error);
      res.status(404).render('naoEncontrado');
      //res.render('resultadoPesquisa'); 
    }
  });
  //res.render('resultadoPesquisa', { title: 'Express' });
});

module.exports = router;