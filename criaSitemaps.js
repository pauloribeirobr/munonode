const request = require('request');
const fs = require('fs');
require('dotenv').config();
let server = process.env.SERVER;

let pagina = 1;

geraSitemap(pagina);
//contabilizaSitemaps(4);

function geraSitemap(pagina) {
    let stream = fs.createWriteStream('public/sitemap-ceps-' + pagina + '.xml', {flags: 'w'}); 
    let options = {
        uri: server + 'cep/gerasitemap/' + pagina,
        method: 'GET',
      };
    console.log('Gerando página ' + pagina);
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            let json = JSON.parse(body);
            //console.log(json.data);
            stream.write('<?xml version="1.0" encoding="UTF-8"?>' + '\r\n');
            stream.write('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' + '\r\n');
            for (var i = 0; i < json.length; i++) {
            stream.write('  <url>' + '\r\n');
            stream.write('    <loc>https://muno.com.br/' + json[i].url + '</loc>\r\n');
            stream.write('    <lastmod>' + json[i].ultimaAtualizacao + '</lastmod>\r\n');
            stream.write('  </url>' + '\r\n');
            console.log("página " + pagina + ". registro " + i);
            }
            stream.write('</urlset>' + '\r\n');
            stream.close();
            stream.on('close', function(){
                geraSitemap(pagina + 1);
            });
        }else{
            contabilizaSitemaps(pagina);
        }
    }); 
}

function contabilizaSitemaps(pagina) {
    let stream = fs.createWriteStream('public/sitemap.xml', {flags: 'w'}); 
    stream.write('<?xml version="1.0" encoding="UTF-8"?>' + '\r\n');
    stream.write('<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' + '\r\n');
    for (var i = 0; i <= pagina; i++) {
        stream.write('  <sitemap>' + '\r\n');
        stream.write('    <loc>https://muno.com.br/sitemap-ceps-' + i + '.xml</loc>\r\n');
        stream.write('    <lastmod>2019-07-01</lastmod>\r\n');
        stream.write('  </sitemap>' + '\r\n');
    }
    stream.write('</sitemapindex>' + '\r\n');
    stream.close();
}
